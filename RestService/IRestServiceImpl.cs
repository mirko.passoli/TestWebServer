﻿using System.ServiceModel;
using System.ServiceModel.Web;

namespace RestService
{
    [ServiceContract]
    public interface IRestServiceImpl
    {
        // Per ogni chiamata diversa devi aggiungere uno di questi blocchi con
        // il nome del metodo che verrà chiamato quando viene effettuata la richiesta

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,    // Quando ritorni una stringa, int...
            UriTemplate = "json/data/{id}")]            // Richiesta all'indirizzo: 
                                                        // http://{indirizzo e porta pc}/RestServiceImpl.svc/json/data/1
                                                        // 1 viene passato come parametro al metodo, 
                                                        // ma deve esserci corrispondenza tra la stringa tra parentesi graffe e nome del parametro
        string JSONData(string id);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Wrapped,
            UriTemplate = "json/data/{id}/{nome}")]     // Richiesta all'indirizzo: 
                                                        // http://{indirizzo e porta pc}/RestServiceImpl.svc/json/data/1/giovanni
                                                        // più parametri
        string JSONData2(string id, string nome);

        [OperationContract]
        [WebInvoke(Method = "GET",
            ResponseFormat = WebMessageFormat.Json,
            BodyStyle = WebMessageBodyStyle.Bare,       // Quando ritorni in formato JSON
            UriTemplate = "json/select/{table}")]
        string Select(string table);
    }
}