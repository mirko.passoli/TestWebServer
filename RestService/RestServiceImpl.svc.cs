﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace RestService
{
    //Implementazione dell'interfaccia

    public class RestServiceImpl : IRestServiceImpl
    {
        const string CONNECTION_STRING = "server=.;database=DATABASE_NAME;user=sa;password=PASSWORD";

        SqlConnection _conn;

        public RestServiceImpl()
        {
            _conn = new SqlConnection();

        }


        #region IRestServiceImpl Members
        public string JSONData(string id)
        {
            return "You requested product " + id;
        }

        public string JSONData2(string id, string nome)
        {
            return "You requested product " + id + ", name: " + nome;
        }

        public string Select(string table)
        {
            SqlDataAdapter da = new SqlDataAdapter($"select * from {table}", _conn);
            DataTable dt = new DataTable();
            da.Fill(dt);

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        #endregion
    }
}